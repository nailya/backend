package com.example.animals;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/animals")
@CrossOrigin
public class AnimalController {
    Logger logger = LoggerFactory.getLogger(AnimalController.class);

    @Autowired
    private AnimalRepository animalRepository;

    @PostMapping
    public Animal addAnimal(@RequestBody Animal animal) {
        return animalRepository.save(animal);
    }

    @GetMapping
    public List<Animal> getAllAnimals() {
        return animalRepository.findAll();
    }

    @GetMapping("/species/{species}")
    public List<Animal> getAnimalsBySpecies(@PathVariable String species) {
        return animalRepository.findBySpecies(species);
    }

    @GetMapping("/name/{name}")
    public List<Animal> getAnimalsByNameContainingIgnoreCase(@PathVariable String name) {
        return animalRepository.findByNameContainingIgnoreCase(name);
    }

}
