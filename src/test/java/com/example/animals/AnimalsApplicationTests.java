package com.example.animals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class AnimalsApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testAddAnimal() throws Exception {
		String animalJson = "{\"name\":\"Lion\",\"species\":\"Big Cat\"}";

		mockMvc.perform(post("/api/animals")
						.contentType(MediaType.APPLICATION_JSON)
						.content(animalJson))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("Lion"))
				.andExpect(jsonPath("$.species").value("Big Cat"));
	}

	@Test
	public void testGetAllAnimals() throws Exception {
		mockMvc.perform(get("/api/animals"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}
}